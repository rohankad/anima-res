﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerGo : MonoBehaviour
{
    [SerializeField]
    private GameObject ButtonPrefab;

    [SerializeField]
    private GameObject ButtonParentGO;

    [SerializeField]
    private List<GameObject> SceneChangeButtons;

    public int ButtonSelected;
    public GameObject Line;
    

    void Awake()
    {
        GameEvents.Begin += CreateButtons;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.Restart += Reset;
        GameEvents.SphereSelected += SwitchToNext;
        GameEvents.ButtonPressed += SwitchToNext;
        StartCoroutine("ClickFirstButtonDefault");
    }

    IEnumerator ClickFirstButtonDefault()
    {
        yield return new WaitForSeconds(1F);
        SwitchToNext(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CreateButtons(int NoOfButtons)
    {
        SceneChangeButtons = new List<GameObject>();
        Debug.Log("Creating Buttons "+NoOfButtons);

        for(int i = 0; i < NoOfButtons; i++)
        {
            GameObject go = Instantiate(ButtonPrefab, ButtonParentGO.transform) as GameObject;
            //go.GetComponent<SceneData>().SceneId = (i+1);
            go.GetComponent<SceneData>().UpdateButtonText(i + 1);
            go.GetComponent<Button>().onClick.AddListener(delegate { SwitchToNext(go.GetComponent<SceneData>().SceneId); });
            SceneChangeButtons.Add(go);
        }

        Line.SetActive(true);
    }

    public void SwitchToNext(int SceneID)
    {

        Debug.Log("ButtonClicked " + SceneID);
        ButtonSelected = SceneID;
        GameEvents.LoadScene?.Invoke(SceneID);
        
       foreach(var btns in SceneChangeButtons)
        {
            if(btns.GetComponent<SceneData>().SceneId== ButtonSelected)
            {
                btns.transform.localScale = new Vector3(1.7f, 1.7f, 1.7f);

            } else
            {
                btns.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            }
        }
    }

    private void Reset(string a) {

        foreach (var btns in SceneChangeButtons)
        {
            btns.transform.localScale = Vector3.one;
        }

        SwitchToNext(1);
    }

}
