﻿using System.Collections;
using System.Collections.Generic;
using System;

public class GameEvents 
{
    public static Action<int> Begin;
    public static Action<int> LoadScene;
    public static Action<bool> SpawnSpheres;
    public static Action<int> SphereSelected;
    public static Action<bool> FadeOutSpheres;
    public static Action<int> ButtonPressed;
    public static Action<string> Restart;
}
