﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    float CameraMovementSpeed;
    Vector3 moveDirection;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            transform.RotateAround(new Vector3(0,0,0), Vector3.up * Input.GetAxis("Mouse X"), 5 * Time.deltaTime);
        }

        moveDirection = (transform.forward * Input.GetAxis("Mouse ScrollWheel")).normalized;
        transform.Translate(moveDirection * CameraMovementSpeed* Time.deltaTime);
    }
}
