﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereManager : MonoBehaviour
{
    [SerializeField]
    private GameObject SpherePrefab;
    
    public int NoOfSpheres;

    public List<GameObject> Spheres;
   
    void Awake()
    {
        GameEvents.SpawnSpheres += SpawnSpheres;
    }
 
    private void SpawnSpheres(bool a)
    {
       
        if (Spheres.Count < 3)
        {
            Debug.Log("--- SpawnSpheres ---");
            Spheres = new List<GameObject>();

            for (int i = 0; i < NoOfSpheres; i++)
            {
                GameObject go = Instantiate(SpherePrefab) as GameObject;

                Spheres.Add(go);
                if (i != 0)
                {
                    go.transform.parent = Spheres[i - 1].transform;
                    float scaleFactor = Spheres[i - 1].transform.localScale.x - 0.25f;
                    go.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);

                    Vector3 tempPos = Spheres[i - 1].transform.position;
                    go.GetComponent<SphereController>().RotateTarget = Spheres[i - 1].transform;
                    go.GetComponent<SphereController>().RotateSpeed = (i + 1) * 10;
                    go.transform.localPosition = new Vector3(1.5f / i, 1, 0);
                }

            }
        }
    }


    public void DeattachFromParent()
    {
        foreach(var a in Spheres)
        {
            a.transform.parent = null;
           if( a.transform.GetChild(0).GetComponent<SphereBehaviour>().SphereSelectedByUser == false)
            {
                Destroy(a);
            }
        }
    }
}
