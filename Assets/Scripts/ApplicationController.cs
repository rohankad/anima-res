﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationController : MonoBehaviour
{
  
    void Start()
    {
        StartCoroutine("MoveSpheretoCenterTrigger");
    }
    IEnumerator MoveSpheretoCenterTrigger()
    {
        yield return new WaitForSeconds(0.5f);
        FindObjectOfType<SphereController>().enabled = true;
        FindObjectOfType<SphereController>().MoveSphere = true;
    }

    public void RestartButtonClicked()
    {
        GameEvents.Restart?.Invoke(this.gameObject.scene.name);
    }



}
