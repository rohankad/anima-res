﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
   private  string[] GameScenes;
    private int ActiveSceneId;

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.Begin?.Invoke(GameScenes.Length);
        GameEvents.LoadScene += LoadScene;
        GameEvents.Restart += RestartApp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LoadScene(int SceneID)
    {
        ActiveSceneId = SceneID;
        Debug.Log("Loading Scene : "+ "Scene" + SceneID);
        AsyncOperation sceneLoader = SceneManager.LoadSceneAsync("Scene" + SceneID, LoadSceneMode.Additive);
       sceneLoader.allowSceneActivation=true;
        sceneLoader.completed += SceneLoadComplete;
       
    }

    private void SceneLoadComplete(AsyncOperation ao)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Scene" + ActiveSceneId));
        Debug.Log("ActiveSceneId : " + ActiveSceneId);
        if (ActiveSceneId == 2)
        {
            GameEvents.SpawnSpheres?.Invoke(true);
        }

        Debug.Log("Scene Load Complete");
        
        for(int i = 0; i < GameScenes.Length; i++)
        {
            if(ActiveSceneId != (i + 1))
            {
                // if (SceneManager.GetActiveScene().name == "Scene" + (i+1)) 
                try
                {
                    SceneManager.UnloadSceneAsync("Scene" + (i + 1));
                } 
                catch(System.Exception e)
                {
                    Debug.Log("No Scene to destroy"+ e.Message);
                }
            }
        }

    }


    public void RestartApp(string SceneToUnload)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Main"));
        SceneManager.UnloadSceneAsync(SceneToUnload);
        ActiveSceneId = 1;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
