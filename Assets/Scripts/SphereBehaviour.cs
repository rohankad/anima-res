﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereBehaviour : MonoBehaviour
{
    public bool SphereSelectedByUser;
    public Animator SphereAnimator;

    // Start is called before the first frame update
    void Start()
    {
        SphereAnimator.SetTrigger("fadein");
        GameEvents.FadeOutSpheres += FadeOutSpheres;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnMouseUp()
    {
        Debug.Log("On Mouse Up");
        if (SphereSelectedByUser == false)
        {
            OnSphereTap();
        }
    }


    void OnSphereTap()
    {
        SphereSelectedByUser = true;
        StartCoroutine("HighlightSphere");
    }

    float t = 0;
    IEnumerator HighlightSphere()
    {
        GameEvents.FadeOutSpheres?.Invoke(true);
        SphereAnimator.SetTrigger("highlight");
        yield return new WaitForSeconds(1.5f);
        this.gameObject.transform.parent.GetComponent<SphereController>().SphereSelected();
    }

    public void FadeOutSpheres(bool a)
    {
        if (!SphereSelectedByUser)
        {
            if(SphereAnimator!=null)
            SphereAnimator.SetTrigger("fadeout");
        }
    }
    
 

}
