﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SphereController : MonoBehaviour
{
    public Transform RotateTarget;
    public float RotateSpeed;
    private float SphereMoveToCenterTime=5f;
    public bool MoveSphere;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.Restart += Reset;
    }

    // Update is called once per frame
    void Update()
    {
       if(RotateTarget!=null)
        {
            transform.RotateAround(RotateTarget.position, Vector3.forward, RotateSpeed * Time.deltaTime);
        }

       if(MoveSphere)
        MoveSpheretoCenter();
    }

   public void SphereSelected()
    {
        // this.transform.parent = null;
        FindObjectOfType<SphereManager>().DeattachFromParent();
        DontDestroyOnLoad(this.gameObject);
        GameEvents.SphereSelected?.Invoke(3);   
    }

    float t = 0;
    void MoveSpheretoCenter()
    {
        
        if (t < SphereMoveToCenterTime)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0, 0, 0), t/SphereMoveToCenterTime);
            t += Time.deltaTime;
        } else
        {
            SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetActiveScene());
            MoveSphere = false;
        }
       
    }


    void Reset(string s)
    {
        if(this!=null)
        Destroy(this.gameObject);
    }
}
