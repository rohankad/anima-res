﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjRotator : MonoBehaviour
{
    public float RotateSpeed;

    // Start is called before the first frame update
    void Start()
    {
      
     
    }

    // Update is called once per frame
    void Update()
    {
  
    }


    void OnMouseDrag()
    {
        float xAxisRot = Input.GetAxis("Mouse X");
        float yAxisRot = Input.GetAxis("Mouse Y") ;

        this.transform.RotateAround(this.transform.position, Vector3.down* xAxisRot, Time.deltaTime * RotateSpeed);
         this.transform.RotateAround(this.transform.position, Vector3.right* yAxisRot, Time.deltaTime * RotateSpeed);
    }

}
