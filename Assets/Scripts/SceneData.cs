﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneData : MonoBehaviour
{
    public int SceneId;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void UpdateButtonText(int ButtonName)
    {
        SceneId = ButtonName;
        transform.GetChild(0).GetComponent<Text>().text = ButtonName.ToString();
    }

}
